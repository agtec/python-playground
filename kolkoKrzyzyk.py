import random

plansza_globalna = [[1, 2, 3],
           [4, 5, 6],
           [7, 8, 9]]



def wyswietl_plansze(plansza):
    a = str(plansza[0][0])
    b = str(plansza[0][1])
    c = str(plansza[0][2])
    d = str(plansza[1][0])
    e = str(plansza[1][1])
    f = str(plansza[1][2])
    g = str(plansza[2][0])
    h = str(plansza[2][1])
    i = str(plansza[2][2])

                
    print("+-------+-------+-------+")
    print("|       |       |       |")
    print("|   "+a+ "   |   "+b+"   |   "+c+"   |")
    print("|       |       |       |")
    print("+-------+-------+-------+")
    print("|       |       |       |")
    print("|   "+d+ "   |   "+e+"   |   "+f+"   |")
    print("|       |       |       |")
    print("+-------+-------+-------+")
    print("|       |       |       |")
    print("|   "+g+ "   |   "+h+"   |   "+i+"   |")
    print("|       |       |       |")
    print("+-------+-------+-------+")

wyswietl_plansze(plansza_globalna)


def ruch_czlowieka(plansza):
    ruch_uzytkownika= int(input("wpisz nr pola od 1 do 9: "))
    while ruch_uzytkownika <= 0 or ruch_uzytkownika >=10:
        ruch_uzytkownika = int(input("wpisz nr pola od 1 do 9: "))
    if ruch_uzytkownika == 1:
        plansza[0][0] = "o"
    if ruch_uzytkownika == 2:
        plansza[0][1] = "o"
    if ruch_uzytkownika == 3:
        plansza[0][2] = "o"
    if ruch_uzytkownika == 4:
        plansza[1][0] = "o"
    if ruch_uzytkownika == 5:
        plansza[1][1] = "o"
    if ruch_uzytkownika == 6:
        plansza[1][2] = "o"
    if ruch_uzytkownika == 7:
        plansza[2][0] = "o"
    if ruch_uzytkownika == 8:
        plansza[2][1] = "o"
    if ruch_uzytkownika == 9:
        plansza[2][2] = "o"
    wyswietl_plansze(plansza)





def ruch_komputera(plansza):
    ruch_komputera=random.randint(1,9)
    while ruch_komputera == ruch_czlowieka:
        ruch_komputera=random.randint(1,9)
    if ruch_komputera == 1:
        plansza[0][0] = "x"
    if ruch_komputera == 2:
        plansza[0][1] = "x"
    if ruch_komputera == 3:
        plansza[0][2] = "x"
    if ruch_komputera == 4:
        plansza[1][0] = "x"
    if ruch_komputera == 5:
        plansza[1][1] = "x"
    if ruch_komputera == 6:
        plansza[1][2] = "x"
    if ruch_komputera == 7:
        plansza[2][0] = "x"
    if ruch_komputera == 8:
        plansza[2][1] = "x"
    if ruch_komputera == 9:
        plansza[2][2] = "x"
    wyswietl_plansze(plansza)
    



def czy_zwyciezca(plansza):
    if plansza[0][0] == "o" and plansza[0][1] == "o" and plansza[0][2] == "o":
        zwyciezca = True
        print("jestes zwyciezca!")
        return True
    if plansza[1][0] == "o" and plansza[1][1] == "o"and plansza[1][2] == "o":
        zwyciezca = True
        print("jestes zwyciezca!")
        return True
    if plansza[2][0] == "o" and plansza[2][1] == "o" and plansza[2][2] == "o":
        zwyciezca = True
        print("jestes zwyciezca!")
        return True
    
    if plansza[0][0] == "x" and plansza[0][1] == "x" and plansza[0][2] == "x":
        zwyciezca = True
        print("komputer jest zwyciezca!")
        return True
    if plansza[1][0] == "x" and plansza[1][1] == "x"and plansza[1][2] == "x":
        zwyciezca = True
        print("komputer jest zwyciezca!")
        return True
    if plansza[2][0] == "x" and plansza[2][1] == "x" and plansza[2][2] == "x":
        zwyciezca = True
        print("komputer jest zwyciezca!")
        return True


while czy_zwyciezca(plansza_globalna) == None:
    ruch_czlowieka(plansza_globalna)
    if czy_zwyciezca(plansza_globalna) == True:
        break
    ruch_komputera(plansza_globalna)
    if czy_zwyciezca(plansza_globalna) == True:
        break
    czy_jest_wolne_pole = False
    for i in range(0, 3):
        for j in range(0, 3):
            if plansza_globalna[i][j] != "o" and plansza_globalna[i][j] != "x":
                czy_jest_wolne_pole = True
    if czy_jest_wolne_pole == False:
        print("nikt nie wygral")
        break
         
    








    
