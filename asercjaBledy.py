
def dziel(l1, l2):
    if l2 == 0:
        raise ZeroDivisionError
    wynik = l1/l2
    return wynik

try:
    dziel(1,0)
except ZeroDivisionError:
    print("dzielenie przez zero")
finally:
    print("udalo sie")

def dzielInaczej(l1, l2):
    assert l2 !=0, "wartosc = 0"
    wynik = l1/l2
    return wynik

print(dzielInaczej(1,0))
