
def czyParzysta(a):
    if a%2==0:
        return True

def zmianaSumy(suma):
    def suma2(*args):
        return suma(*args) + 1
    return suma2

@zmianaSumy
def suma(a,b):
    return a+b

print(suma(1,3))
