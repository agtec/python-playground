def intersect(*args):
    lista=[]
    for x in args[0]:
        for other in args[1:]:
            if x not in other:
                break
            else:
                lista.append(x)
    return lista

l1 = [1, 2, 5, 7, 0]
l2 = [1, 3, 2, 8, 7]
print(intersect(l1, l2))
