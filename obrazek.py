class Obrazek:
    def __init__(self, filename):
        self.filename = filename
class ObrazekJpeg(Obrazek):
    def obejrzyj(self):
        print("ogladasz jpeg" + self.filename)
class ObrazekPng(Obrazek):
    def obejrzyj(self):
        print("ogladasz png" + self.filename)

obrazki = [ObrazekJpeg("Pies,jpeg"), ObrazekPng("Kot.png")]

obrazki[0].obejrzyj()
obrazki[1].obejrzyj()

for obrazek in obrazki:
    obrazek.obejrzyj()
