#enumerate

seasons = ['summer', 'spring', 'fall', 'winter']


print(list(enumerate(seasons)))

for counter,value in enumerate(seasons):
    print(counter, value)

for counter, value in enumerate(seasons, 100):
    print(counter, value)

#dir

import struct

dir()

# set()

lista = [1, 2, 2, 3, 4, 1, 5, 6]

zbior = set(lista)

print(zbior)


lista2 = list(zbior)


print(lista2)

lista3 = ["a", "b", "c"]

zbior2 = set(lista3)

print(zbior.union(zbior2))
print(zbior.difference(zbior2))
print(zbior.intersection(zbior2))

zbior2.add(1)

print(zbior2)

print(zbior.issubset(zbior2))

zbior.remove(1)

print(zbior)

# zbior.clear() - usuwa wszytsko



# zip()


x = zip("abcdef", [1,2,3,4,5])

y = list(x)

print(y)

#map()

xx = map(int, [2.3, 4.5])

print(list(xx))

from functools import reduce

#reduce

def suma(x1, x2):
    return(x1+x2)

print(reduce(suma, [1,2,3]))

#lambda:

mnozenie = lambda x,y: x*y

wynik1 = mnozenie(1,2)
print(wynik1)

#from functools import apply

#apply

##def dzielenie(x,y):
##    return x//y
##
##apply(dzielenie, (1,2))
##














