##Ostatnia cyfra numeru PESEL jest tzw. cyfrą kontrolną i służy do weryfikacji poprawności numeru. Korzysta się z niej następująco:
##oblicza się wartość wyrażenia  c1 + 3c2 + 7c3 + 9c4 + c5 + 3c6 + 7c7 + 9c8 + c9 + 3c10 + c11 (gdzie ci to i-ta cyfra numeru PESEL)
##sprawdza się ostatnią cyfrę otrzymanego wyniku - jeśli jest to 0, PESEL jest poprawny; jeśli nie, PESEL jest błędny

pesel = input("Podaj pesel: ")

c1 = int(pesel[0])
c2 = int(pesel[1])
c3 = int(pesel[2])
c4 = int(pesel[3])
c5 = int(pesel[4])
c6 = int(pesel[5])
c7 = int(pesel[6])
c8 = int(pesel[7])
c9 = int(pesel[8])
c10 = int(pesel[9])
c11 = int(pesel[10])

wynik = c1 + (3*c2) + (7*c3) + (9*c4) + c5 + (3*c6) + (7*c7) + (9*c8) + c9 + (3*c10) + c11

print(wynik)

wynik = str(wynik)

ostatnia_cyfra = wynik[-1]
print(ostatnia_cyfra)

if ostatnia_cyfra == 0:
    print("pesel poprawny")
else:
    print("pesel niepoprawny")

lista = []

for i in range(0, len(pesel)):
    a1 = int(pesel[i])
    lista.append(a1)

print(lista)


    
