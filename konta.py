class Konto:
    def __init__(self, imie, nazwisko, nrKonta, stanKonta):
        self.imie = imie
        self.nazwisko = nazwisko
        self.nrKonta = nrKonta
        self.stanKonta = stanKonta
    def wyswietlInfo(self):
        print(self.imie + " " + self.nazwisko + " " + str(self.nrKonta))
    def wplac(self, obiekt_konto, kwota):
        self.stanKonta -= kwota
        obiekt_konto.przyjmij(kwota)
        print("wplacono " + str(kwota) + ",stan konta: " + str(self.stanKonta))
    def przyjmij(self, kwota):
        self.stanKonta += kwota
        return self.stanKonta
    def zapis(self):
        konto = open("konta.txt", "w")
        try:
            konto.write(self.imie + " " + self.nazwisko + " " + self.nrKonta)
        finally:
            konto.close()
    def odczyt(self):
        konto = open("konto.txt")
        try:
            tekst = konto.read()
        finally:
            konto.close()
        print(tekst)


class KontoFirmowe(Konto):
    def __init__(self, nazwa, nrKonta, stanKonta):
        super().__init__("", "", nrKonta,stanKonta)
        self.nazwa = nazwa
    def oplacZUS(self):
        if(self.stanKonta >= 1000):
            self.stanKonta = self.stanKonta - 1000
            return self.stanKonta
        else:
            print("brak srodkow")

class KontoPrywatne(Konto):
    def wyswietlInfo(self):
        super().wyswietlInfo()
        print(self.stanKonta)
    def zaplac(self):
        pass


kp = KontoPrywatne("a", "b", 123, 20)
kp.wyswietlInfo()

kf = KontoFirmowe("x", 555, 10)
print(kf.oplacZUS())
kf.wyswietlInfo()

kp.wplac(kf, 5)
print(kp.stanKonta)
        
