class Kalendarz:
    def __init__(self, rok, miesiac):
        self.rok = rok
        self.miesiac = miesiac
    def czyPrzestepny(self, rok):
        if (self.rok % 4 == 0 and self.rok % 100 != 0 and self.rok % 400 == 0):
            return true
    def ileDni(self, miesiac):
        miesiace = ["styczen", "luty", "marzec", "kwiecien", "maj", "czerwiec", "lipiec", "sierpien", "wrzesien", "pazdziernik", "listopad", "grudzien"]
        liczba_dni = [31, 29, 30, 31, 30, 31, 31, 30, 31, 30, 31, 31]
        if (miesiac != "luty" and miesiac in miesiace):
            return liczba_dni[miesiace.indexOf(miesiac)]
        else:
            if (miesiac == "luty" and self.czyPrzestepny(self.rok)):
                return liczba_dni[miesiace.indexOf(miesiac)]
            else:
                return 28
    def pokaz(self, miesiac, rok):
        self.czyPrzestepny(rok)
        liczbaDni = self.ileDni(miesiac)
        iloscWierszy = 1
        for i in range (1, liczbaDni+1):
            iloscWierszy+=1
            if iloscWierszy % 7 != 0:
                print(str(i) + " ", end="")
            else:
                print(i)


k1 = Kalendarz(1999, 2)
k1.pokaz(2, 1999)
            
        
        
